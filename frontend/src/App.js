import NavBar from './components/NavBar'
import FormDebts from './components/FormDebts'

function App() {
  return (
    <div className="App">
	  <NavBar/>
	  <FormDebts/>
    </div>
  );
}

export default App;
