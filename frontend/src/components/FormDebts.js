import React from 'react';
import axios from 'axios';
import '../css/form_debts.css';

class FormDebts extends React.Component {
	constructor(){
		super();
		this.state = {
		license_plate: "",
		renavam: "",
		debts_option: "all",
		response: {},
		result: "",
	};
	this.handlerSubmit = this.handlerSubmit.bind(this);
	this.changeHandler = this.changeHandler.bind(this);
	}
	
	changeHandler(event){
		this.setState({
			[event.target.name]: event.target.value,
		});
	}

	handlerSubmit(){
		let params = "?license_plate=" + this.state.license_plate + "&renavam=" + this.state.renavam +"&debt_option=" + this.state.debts_option;
		axios.get('http://localhost:8080/debts/'+params)
			.then(response => {
				this.setState({response: response.data});
			})
	}
	
	resultPrint(){
		console.log(this.state.response);
		console.log(this.state.debts_option);
		if(Object.keys(this.state.response).length === 0){
			return (<h1>Nenhuma Consulta</h1>)
		}
		if("status" in this.state.response){
			return (
				<h1>{this.state.response["message"]}</h1>
			)
		}
		if(this.state.debts_option === "all"){
			var data = [];
				data.push(<h1>Ticket</h1>);
				for(var each in this.state.response["ticket"]){
					for(var index in this.state.response["ticket"][each]){
						data.push(<h2>{index}: {this.state.response["ticket"][each][index]}</h2>);
					}
				}

				data.push(<h1>IPVA</h1>);
				for(var each in this.state.response["ipva"]){
					for(var index in this.state.response["ipva"][each]){
							data.push(<h2>{index}: {this.state.response["ipva"][each][index]}</h2>);
						}
				}

				data.push(<h1>DPVAT</h1>)
				for(var each in this.state.response["dpvat"]){
					for(var index in this.state.response["dpvat"][each]){
						data.push(<h2>{index}: {this.state.response["dpvat"][each][index]}</h2>);
					}
				}

				data.push(<h1>Licenciamento</h1>);
				for(var each in this.state.response["licensing"]){
					for(var index in this.state.response["licensing"][each]){
						if(index === "description"){
							for(var sub_index in this.state.response["licensing"][each]["description"]){
								data.push(<h2>{sub_index}: {this.state.response["licensing"][each]["description"][sub_index]}</h2>)
							}
						}
						else{
						data.push(<h2>{index}: {this.state.response["licensing"][each][index]}</h2>)
					}
					}
				}
				return(data);
			}
		if(this.state.debts_option === "ticket"){
			var data = [];
			data.push(<h1>Ticket</h1>);
			for(var each in this.state.response){
				for(var index in this.state.response[each]){
					data.push(<h2>{index}: {this.state.response[each][index]}</h2>);
				}
			}
			return(data);
		}

		if(this.state.debts_option === "ipva"){
			var data = [];
				data.push(<h1>IPVA</h1>);
				for(var each in this.state.response){
					for(var index in this.state.response[each]){
						data.push(<h2>{index}: {this.state.response[each][index]}</h2>);
					}
				}
				return(data);
		}

		if(this.state.debts_option === "dpvat"){
			var data = [];
				data.push(<h1>DPVAT</h1>);
				for(var each in this.state.response){
					for(var index in this.state.response[each]){
						data.push(<h2>{index}: {this.state.response[each][index]}</h2>);
					}
				}
				return(data);
		}

		if(this.state.debts_option === "licensing"){
			var data = [];
			data.push(<h1>Licenciamento</h1>);
			for(var each in this.state.response){
				for(var index in this.state.response[each]){
					if(index === "description"){
						for(var sub_index in this.state.response[each]["description"]){
							data.push(<h2>{sub_index}: {this.state.response[each]["description"][sub_index]}</h2>)
						}
					}
					else{
					data.push(<h2>{index}: {this.state.response[each][index]}</h2>)
				}
				}
			}
			return(data);
		}

		return (<h1>Sem resultado</h1>)
	}

	render() {
		let result = this.resultPrint();
		return (
		<div className="form-submit">
			<div>
					<label className="label-form">Placa do Carro</label>
					<br />
					<input
						value={this.state.license_plate}
						name="license_plate"
						onChange={this.changeHandler}
						type="text"
						className="input-form"
					/>
					<br/>
					<label className="label-form">Renavam</label>
					<br />
					<input
						value={this.state.renavam}
						name="renavam"
						onChange={this.changeHandler}
						type="text"
						className="input-form"
					/>
					<br/>
					<label className="label-form">Opção de consulta</label>
					<br />
					<select
						value={this.state.debts_option}
						onChange={this.changeHandler}
						name="debts_option"
					>
						<option value="all"> All </option>
						<option value="ticket"> Ticket </option>
						<option value="ipva"> IPVA </option>
						<option value="dpvat"> DPVAT </option>
						<option value="licensing"> Licenciamento </option>
					</select>
					<button type="submit" onClick={() => this.handlerSubmit()}>Consulta</button>
			</div>
				<div className="result">
					{result}
				</div>
		</div>
		)
	}
}
export default FormDebts;
