# desafio-zapay

Projeto para resolver o desafio do processo seletivo da zapay


## Pré requisitos
* Docker
* Docker-compose

## Build e Execução

build: `docker-compose build`;

run: `docker-compose up`;

backend: [backend](http://localhost:8080);

frontend: [frontend](http://localhost:3000);

## Explicações do projeto

O projeto tem duas partes uma em django responsável pela API que se comunica com
a API do detran e devolve as informações assim como requirido. o formato da requisição é json
e tem como argumentos `license_plate`. `renavam` e `debt_option`. O endpoint da api é de **GET** e
está contido em [http://localhost:8080/debts/](http://localhost:8080/debts/) onde é possivel ver o
response na view do django restframework. Para realização do problem foi identificado que poderia ser
usado a arquitetura comportamental command que consiste na utilização de uma abstração de commandos que
podem ser mudados seu funcionamento. Isso ajuda na reutilização do código bem como na implementação de 
novas consultas.

~~~Json
{
	"license_plate": "ABC1C34",
	"renavam": "11111111111",
	"debt_option": "<opcional>" //porém pode ter os valores de ticket, ipva, dpvat e licensing.
}
~~~


Nesse projeto foi utilizado React para o frontend onde tem apenas uma tela onde apresenta campos para serem
preenchidos de acordo com os parametros das pequisas do backend. Logo após a consulta o resultado aparece abaixo dos campos.

![tela_front](imgs/tela_front.png)

O projeto apresenta CI(continuos integrations) descrita no .gitlab-ci.yml em que apresenta a etapas de teste(tanto de style sheet quanto os testes unitários(pytest)), build para a aplicação de backend e release onde a imagem do serviço de backend é dado push para os containers registry do gitlab onde baseada na branch é tagueada como devel(homolog) ou lastest/master(production). Para verificar o CI vai em CI/CD na barra lateral do gitlab e depois em pipelines.
![pipelines](imgs/pipelines.png)
![containers](imgs/containers.png)

No docker-compose há 3 imagens (backend, frontend e banco de dados). O Banco de dados nesse caso não foi necessário porém para mostrar conhecimento no uso e integração de bancos com APIs essa feature foi implementada.

Para melhor melhor execução do projeto foi feito baselines ou releases baseados nas tags para cada grande marco no projeto. Nesse caso foi utilizado as proprias labels do gitlab com versões onde x.y.z, x é baseline, y feature implementada, z bug corrigido. Para acesse-las basta ir em repositório -> tags.
![tags](imgs/tags.png)

Para realização do projeto foram seguidas algumas diretrizes de contribuição as issues eram númeradas conforme sua criação com título separado por "-" exemplo 01-Criar-ambiente-de-desenvolvimento e as branchs para realização
dessa issues apresentavam o mesmo nome da issue. Foi utilizado o kaban do gilab para guiar o fluxo de trabalho. Todas as issues levantadas e realizados foram baseadas nas instruções do desafio. Para melhor guiar o desenvolverdor nos commits de cada funcionalidade foi utilizado a opção #<número da issue> dessa forma nas issues estão listadas todos os commits que foram realizados para atende-la.
![issues](imgs/issues.png)
![commits](imgs/commits.png)

Por fim foram colocadas bands para conseguir visualizar a cobertura de testes da aplicação bem como o estado da pipeline. Todos os MR(merge request) foram feitos com o critério de aprovação com a pipeline com sucesso e um review aprovado.
![bands](imgs/bands.png)
![MR](imgs/MR.png)
