from abc import ABC, abstractmethod
from debts.api import API


class ABCRequest(ABC):
    """
        Classe abstrata para ser herdada por todas as classes de requests
    """
    def __init__(self, **kwargs):
        """
        Construtor da classe abstrata.
        """
        self.params = kwargs

    @abstractmethod
    def debt_rule(self, debt):
        """
            Método responsável por implementar o formato do json para ser
            mostrado para o cliente na consulta.
        """
        raise NotImplementedError

    def parser(self, debts):
        """
            Método que implementa o parse e aplica ele em todas os itens da
            lista da consulta.
        """
        if debts is None:
            return []
        parser_data = list(
            map(
                self.debt_rule,
                debts[self.params["sub_key"]]
            )
        )
        return parser_data

    def get_detran_data(self, option):
        """
            Método para fazer a requisição para a api
        """
        api = API(
            self.params["license_plate"],
            self.params["renavam"],
            option
        )
        return api.fetch()

    def run(self):
        """
            Método para ser acionado e devolver a consulta já no formato
            correto.
        """
        response = self.get_detran_data(
            self.params["consult"]
        )
        parser_data = self.parser(
            response.get(self.params["key"] or None)
        )
        return parser_data
