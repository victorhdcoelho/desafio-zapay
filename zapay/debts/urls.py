from debts.views import GetDebts
from django.urls import path

urlpatterns = [
    path('debts/', GetDebts.as_view())
]
