from debts.abc_request import ABCRequest


class TicketRequest(ABCRequest):
    """
        Classe responsável por implementar o request do ticket
    """

    def debt_rule(self, debt):
        """
            Método que implementa a regra de formatação dos debitos das
            multas.
        """
        return {
                'amount': float(debt.get('Valor'))/100,
                'auto_infraction': debt.get('AIIP'),
                'description': debt.get('DescricaoEnquadramento'),
                'title': 'Infração de Trânsito',
                'type': "ticket",
        }


class IPVARequest(ABCRequest):
    """
        Classe responsável por implementar o request do IPVA
    """

    def debt_rule(self, debt):
        """
            Método que implementa a regra para a formatação dos debitos do
            IPVA.
        """
        year = debt.get('Exercicio')
        description = f"IPVA {debt.get('Exercicio')}"
        installment = debt.get('Cota', None)
        title = "- Cota " \
                f"{'Única' if installment in [7, 8, 0] else installment}"

        to_collection = {
            'amount': float(debt.get('Valor'))/100,
            'description': description,
            'title': f"IPVA {title}",
            'type': 'ipva',
            'year': year,
        }

        if installment is not None:
            to_collection['installment'] = 'unique' if installment in \
                                            [0, 7, 8] else installment
        return to_collection


class DPVATRequest(ABCRequest):
    """
        Classe responsável por fazer os requests de dpvat
    """

    def debt_rule(self, debt):
        """
            Método que implementa a regra para a formatação dos debitos do
            DPVAT.
        """
        return {
            'amount': float(debt.get('Valor'))/100,
            'description': debt.get(
                    'DescricaoServico',
                    f"DPVAT {debt['Exercicio']}"
                ),
            'title': 'Seguro Obrigatório',
            'type': 'insurance',
            'year': debt.get('Exercicio'),
        }


class LicensingRequest(ABCRequest):
    """
        Classe responsável por fazer o request do licenciamento.
    """
    def debt_rule(self, debt):
        """
            Método que implementa a regra para a formatação dos débitos do
            licenciamento.
        """
        return [{
            "amount": float(debt.get("TaxaLicenciamento"))/100,
            "title": debt.get("Servico"),
            "description": debt.get("Veiculo"),
            "year": debt.get("Exercicio"),
            "type": "licensing",
        }]

    def run(self):
        """
            Sobrescrita do método run para se ajustar a nova opção do debito.
        """
        response = self.get_detran_data(
            self.params["consult"]
        )
        parsed_data = self.debt_rule(response)
        return parsed_data
