from debts.requests_commands import (
    TicketRequest,
    IPVARequest,
    DPVATRequest,
    LicensingRequest
)

"""
    Dicionário responsável pela abstração das regras dos requests.
    E responsável por acionar todas as runs da arquitetura comportamental
    command: https://refactoring.guru/design-patterns/command.
"""
DICT_RUNS = {
    "ticket": {
        "func": TicketRequest,
        "consult": "ConsultaMultas",
        "key": "Multas",
        "sub_key": "Multa",
    },
    "ipva": {
        "func": IPVARequest,
        "consult": "ConsultaIPVA",
        "key": "IPVAs",
        "sub_key": "IPVA",
    },
    "dpvat": {
        "func": DPVATRequest,
        "consult": "ConsultaDPVAT",
        "key": "DPVATs",
        "sub_key": "DPVAT",
    },
    "licensing": {
        "func": LicensingRequest,
        "consult": "ConsultaLicenciamento",
        "key": None,
        "sub_key": None,
    }
}


class CollectRequests:
    """
        Classe responsável por rodar todos os comandos conforme o
        comportamento adequado para cada um deles. Neste caso caso a opção
        não for definida ele rodará todas as regras definidas para as
        requisições
    """
    def __init__(self, **kwargs):
        """
            Método construtor
        """
        self.params = kwargs

    def run_all(self):
        """
            Métodos responśavel por rodar todas as regras e devolver para
            o endpoint definido.
        """
        if self.params["debt_option"] in DICT_RUNS.keys():
            return DICT_RUNS[self.params["debt_option"]]["func"](
                license_plate=self.params["license_plate"],
                renavam=self.params["renavam"],
                consult=DICT_RUNS[self.params["debt_option"]]["consult"],
                key=DICT_RUNS[self.params["debt_option"]]["key"],
                sub_key=DICT_RUNS[self.params["debt_option"]]["sub_key"],
            ).run()
        else:
            all_results = {}
            for key in DICT_RUNS.keys():
                all_results[key] = DICT_RUNS[key]["func"](
                    license_plate=self.params["license_plate"],
                    renavam=self.params["renavam"],
                    consult=DICT_RUNS[key]["consult"],
                    key=DICT_RUNS[key]["key"],
                    sub_key=DICT_RUNS[key]["sub_key"],
                    ).run()

            return all_results
