def fix_plate(plate):
    """
        Função para que recebe a placa do carro em qualquer versão e devolve
        ela corrigida em caso de ser do mercosul.
    """
    plate = list(plate)
    letter_to_num = {}
    for num, letter in enumerate("ABCDEFGHI"):
        letter_to_num[letter] = num

    try:
        plate[4] = str(letter_to_num[plate[4].upper()])
        return "".join(plate)
    except Exception:
        return "".join(plate)
