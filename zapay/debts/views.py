from debts.collect_requests import CollectRequests
from debts.utils import fix_plate
from rest_framework.response import Response
from rest_framework.views import APIView


class GetDebts(APIView):
    """
        View para realizar a consulta de devolver o resultado via json.

        POST:
            debt_option: Opções de consulta de debito (ticket, ipva, dpvat)
            license_plate: Placa do veículo
            renavam: Número do documento do renavam
    """
    def get(self, request, format=None):
        metadata = request.GET
        print(metadata)
        try:
            result = CollectRequests(
                license_plate=fix_plate(metadata["license_plate"]),
                renavam=metadata["renavam"],
                debt_option=metadata.get("debt_option") or "all"
            ).run_all()

        except Exception as exc:
            return Response({
                "status": 500,
                "message": "Invalid Option {}".format(exc)
            })
        return Response(result)
