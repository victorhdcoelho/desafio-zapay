from debts.requests_commands import (
    TicketRequest,
    DPVATRequest,
    IPVARequest,
    LicensingRequest
)
from debts.utils import fix_plate
from debts.mocks import (
    metadata,
    API,
    RESULT
)


def test_ipva_result():
    ipva = IPVARequest(
        license_plate=metadata["license_plate"],
        renavam=metadata["renavam"],
        consult="ConsultaIPVA",
        sub_key="IPVA"
    )
    result = ipva.parser(API["ipva"]["IPVAs"])
    assert result == RESULT["ipva"]


def test_dpvat_result():
    dpvat = DPVATRequest(
        license_plate=metadata["license_plate"],
        renavam=metadata["renavam"],
        consult="ConsultaDPVAT",
        sub_key="DPVAT"
    )
    result = dpvat.parser(API["dpvat"]["DPVATs"])
    assert result == RESULT["dpvat"]


def test_ticket_result():
    ticket = TicketRequest(
        license_plate=metadata["license_plate"],
        renavam=metadata["renavam"],
        consult="ConsultaMultas",
        sub_key="Multa"
    )
    result = ticket.parser(API["ticket"]["Multas"])
    assert result == RESULT["ticket"]


def test_licensing_result():
    licensing = LicensingRequest(
        license_plate=metadata["license_plate"],
        renavam=metadata["renavam"],
        consult="ConsultaLicenciamento",
    )
    result = licensing.debt_rule(API["licensing"])
    assert result == RESULT["licensing"]


def test_fix_plate():
    plate = "ABC1C34"
    new_plate = fix_plate(plate)
    assert new_plate == metadata["license_plate"]
